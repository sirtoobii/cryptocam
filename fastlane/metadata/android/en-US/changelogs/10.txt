BIG UPDATE:

Cryptocam no longer uses OpenPGP or ffmpeg to encrypt videos. That means:
 - better encryption that is impossible to configure insecurely
 - key import by scanning qr codes instead of copying keyfiles
 - no dependency on OpenKeychain anymore
 - less bugs and crashes due to interaction with OpenKeychain
 - much smaller APK download

A new version of the Cryptocam Companion app is required to generate keys and decrypt videos recorded
with this version.
